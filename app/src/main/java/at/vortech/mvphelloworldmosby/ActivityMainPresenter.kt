package at.vortech.mvphelloworldmosby

import android.util.Log
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter


class ActivityMainPresenter : MvpBasePresenter<ActivityMainView>() {

    fun setTextToHelloWorld() {
        Log.i("TAG", "SETTEXTTOHELLOWORLD CALLED")
        if (isViewAttached)
            view!!.setTextToTextView("HelloWorld")
    }

    fun resetText() {
        if (isViewAttached)
            view!!.setTextToTextView("")
    }

}