package at.vortech.mvphelloworldmosby

import android.os.Bundle
import com.hannesdorfmann.mosby.mvp.viewstate.RestorableViewState


class ActivityMainViewState : RestorableViewState<ActivityMainView> {

    private val TEXT_VIEW_KEY = "text_view_key"
    var textViewText: String = ""


    override fun saveInstanceState(out: Bundle) {
        out.putString(TEXT_VIEW_KEY, textViewText)
    }

    override fun restoreInstanceState(`in`: Bundle?): RestorableViewState<ActivityMainView>? {
        if (`in` != null) {
            textViewText = `in`!!.getString(TEXT_VIEW_KEY)
            return this
        } else {
            return null
        }
    }

    override fun apply(view: ActivityMainView?, retained: Boolean) {
        view?.setTextToTextView(textViewText)
    }
}