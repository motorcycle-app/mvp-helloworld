package at.vortech.mvphelloworldmosby

import com.hannesdorfmann.mosby.mvp.MvpView


interface ActivityMainView : MvpView {
    fun setTextToTextView(text: String): Unit
}