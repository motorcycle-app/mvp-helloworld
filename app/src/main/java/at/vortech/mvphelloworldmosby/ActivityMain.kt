package at.vortech.mvphelloworldmosby

import android.os.Bundle
import com.hannesdorfmann.mosby.mvp.viewstate.MvpViewStateActivity
import kotlinx.android.synthetic.main.activity_main.*

class ActivityMain : MvpViewStateActivity<ActivityMainView, ActivityMainPresenter>(), ActivityMainView {

    override fun createViewState() = ActivityMainViewState()

    //what do we want to do on first startup, when there is no viewstate yet
    override fun onNewViewStateInstance() {
        //foo
    }

    //called internally by mosby
    override fun createPresenter() = ActivityMainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_main_settext.setOnClickListener {
            presenter.setTextToHelloWorld()
        }
        activity_main_cleartext.setOnClickListener {
            presenter.resetText()
        }
    }

    override fun setTextToTextView(text: String) {
        activity_main_text.text = text
        val myViewState = viewState as ActivityMainViewState
        myViewState.textViewText = text
    }
}
